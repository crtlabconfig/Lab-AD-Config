# DSC requires explicit confirmation before storing passwords insecurely
$ConfigurationData = @{
    AllNodes = @(
        @{
            NodeName = "${ENV:ComputerName}"
            PSDscAllowPlainTextPassword = $true  
            PSDscAllowDomainUser = $true     
            Credential = $DACreds }
        )
}


configuration Lab_DC_Build
{
 param
    (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [System.Management.Automation.PSCredential]
        $Password
    )


    Import-DscResource -Module ActiveDirectoryDsc

    Node "${ENV:ComputerName}"
    {
        ADOrganizationalUnit 'VMware' {
           Name = "VMware"
           Path = $BaseDomainDN
           ProtectedFromAccidentalDeletion = $True
           Description = "VMware Master OU"
           Ensure = "Present"
                 }
        ADOrganizationalUnit "VMwareUsers" {
           Name = "Users"
           Path = $VMwareBaseDN
           ProtectedFromAccidentalDeletion = $True
           Description = "VMware Specific USer Accounts"
           Ensure = "Present"
        }
        ADOrganizationalUnit "VMWareGroups" {
            Name = "Groups"
            Path = $VMwareBaseDN
            ProtectedFromAccidentalDeletion = $True
            Description = "VMware Specific Groups"
            Ensure = "Present"
        }

        ADUser "JDoe" {
            UserName = "jdoe"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "jdoe"
            DisplayName = "Jane Doe"
            GivenName = "Jane"
            Surname =  "Doe"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "jdoe@$EmailDomain"		
        }

        ADUser "CarsAdmin" {
            UserName = "cars"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "cars"
            DisplayName = "Carlos Tronco - Admin"
            GivenName = "Carlos"
            Surname =  "Tronco-Admin"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "cars@$EmailDomain"		
        }

        ADUser "VRALDap" {
            UserName = "svcvraldap"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcvraldap"
            DisplayName = "Service VRA LDAP"
            GivenName = "Service"
            Surname =  "VRALDAP"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcvraldap@$EmailDomain"
        }

        ADUser "VCenterLdap" {
            UserName = "svcvcldap"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcvcldap"
            DisplayName = "Service VCSA LDAP"
            GivenName = "Service"
            Surname =  "VCLDAP"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcvcldap@$EmailDomain"
        }

        ADUser "VRAIaaS" {
            UserName = "svcvra"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcvra"
            DisplayName = "Service VRA IaaS USer"
            GivenName = "Service"
            Surname =  "VRA"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcvra@$EmailDomain"
        }

        ADUser "vRLCM" {
            UserName = "svcvrlcm"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcvrlcm"
            DisplayName = "vRLifecycleManager"
            GivenName = "Service"
            Surname =  "VRlcm"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcvrlcm@$EmailDomain"
        }

        ADUser "vRAJenkins" {
            UserName = "vraJenkins"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "vRAJenkins"
            DisplayName = "vRA Jenkins Test Acct"
            GivenName = "vRA"
            Surname =  "Jenkins"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "vrajenkins@$EmailDomain"
        }

      ADUser "JenkinsVCenter" {
            UserName = "svcjenkinsvc"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "Jenkins vCenter"
            DisplayName = "Jenkins vCentet"
            GivenName = "vcenter"
            Surname =  "Jenkins"
            PasswordNeverExpires = $True
            Path = $VMwareUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcjenkinsvc@$EmailDomain"
        }



        ADUser "Zabbix" {
            UserName = "Zabbix Server"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcZabbix"
            DisplayName = "Zabbix Service Account"
            GivenName = "Service"
            Surname =  "Zabbix"
            PasswordNeverExpires = $True
            Path = $DefaultUsersDN
            DomainName = $DomainName
	    EmailAddress = "svczabbix@$EmailDomain"
        }

        ADUser "Packer" {
            UserName = "Service Packer"
            Password = $Password
            Ensure = "Present"
            Enabled = $True
            CommonName = "svcPacker"
            DisplayName = "Zabbix Service Account"
            GivenName = "Service"
            Surname =  "Packer"
            PasswordNeverExpires = $True
            Path = $DefaultUsersDN
            DomainName = $DomainName
	    EmailAddress = "svcpacker@$EmailDomain"
        }


        ADGroup "VMwareAdmins"
        {
           GroupName = "VMWareAdmins"
           GroupScope = "Global"
           Category = "Security"
           Description = "VMware Admin Account"
           Path = $VMwareGroupsDN
           Ensure = 'Present'
           Members  = "Cars","SvcVRALDAP"
        }

        ADGroup "DomainAdmins"
        {
           GroupName = "Domain Admins"
           Ensure = 'Present'
           Members = "Cars", "administrator"
        }

        ADGroup "Admins"
        {
           GroupName = "Administrators"
           Ensure = 'Present'
           GroupScope = 'DomainLocal'
           Members = "Cars", "administrator","enterprise admins","domain admins"

        }
    }
}
